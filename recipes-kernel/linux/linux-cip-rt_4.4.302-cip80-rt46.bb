#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2019 - 2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-rt-common.inc

SRC_URI[sha256sum] = "2195422a0d154d8191e6c974a9946b3c91187dde891c52ae971e39ccacf9556e"
