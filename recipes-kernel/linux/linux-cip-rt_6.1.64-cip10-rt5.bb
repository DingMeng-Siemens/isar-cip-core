#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2023
#
# SPDX-License-Identifier: MIT

require linux-cip-common.inc

SRC_URI[sha256sum] = "9dfe41c5975565094399798d715bf0c246b318348b600c480eaeb87f0668cc39"
