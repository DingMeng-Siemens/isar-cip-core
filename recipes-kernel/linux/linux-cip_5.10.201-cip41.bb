#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2021-2023
#
# Authors:
#  Jan Kiszka <jan.kiszka@siemens.com>
#
# SPDX-License-Identifier: MIT
#

require linux-cip-common.inc

SRC_URI[sha256sum] = "40e976196aa52c07a9bd39e9d7d320919aac8978a518de83680223aa04f0d11f"
