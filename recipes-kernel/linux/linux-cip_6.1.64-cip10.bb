#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2023
#
# SPDX-License-Identifier: MIT

require linux-cip-common.inc

SRC_URI[sha256sum] = "4439d9cadbe7dfa91490d883270636e6f8f2fb8796dbd4e01356e851f106d90e"
